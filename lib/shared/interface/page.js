/*jshint -W098*/
'use strict';

/**
 * Represents a page.
 * @interface
 */
function IPage() {
  throw new Error('Not implemented.');
}

// --

/**
 * Contains the address.
 * @type {string}
 */
IPage.address = String();

/**
 * Contains the number.
 * @type {number}
 */
IPage.number = Number();

// --

/**
 * Retrieves the image address.
 * @param {?} $
 * @returns {(?string|Array.<string>)}
 */
IPage.imageAddress = function($) {
  throw new Error('Not implemented.');
};

// --

module.exports = IPage;
